print('''


_____________________    _____  _________ ___________.____       _____ _____.___.    _____    ___________________ __________________
\__    ___/\______   \  /  _  \ \_   ___ \\_   _____/|    |     /  _  \\__  |   |   /  _  \  /  _____/\_   _____/ \      \__    ___/
  |    |    |       _/ /  /_\  \/    \  \/ |    __)_ |    |    /  /_\  \/   |   |  /  /_\  \/   \  ___ |    __)_  /   |   \|    |   
  |    |    |    |   \/    |    \     \____|        \|    |___/    |    \____   | /    |    \    \_\  \|        \/    |    \    |   
  |____|    |____|_  /\____|__  /\______  /_______  /|_______ \____|__  / ______| \____|__  /\______  /_______  /\____|__  /____|   
                   \/         \/        \/        \/         \/       \/\/                \/        \/        \/         \/         
''')

import urllib.request
from zipfile import ZipFile 
import os
import ctypes
import sys, traceback
import shutil
import subprocess
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove
import tarfile

class AdminStateUnknownError(Exception):
    """Cannot determine whether the user is an admin."""
    pass

def is_user_admin():
    try:
        return os.getuid() == 0
            
    except AttributeError:
        pass
    try:
        return ctypes.windll.shell32.IsUserAnAdmin() == 1
            
    except AttributeError:
        raise AdminStateUnknownError
   
if(is_user_admin() == False):
    print ("Sorry. This script requires sudo privledges")
    sys.exit()


def main():
    
        my_current_directory = os.getcwd()
        responsefind,findir=search_directory(my_current_directory)

        while True:
            print('''
1)Windows
2)Linux  
3)Exit
            ''')
            os_choice = input("\033[1;36mkat > \033[1;m")
            print(os_choice)
            if(os_choice=='3'):
                sys.exit()
            while (os_choice=='1'):
                print('''
1)install Winlogbeat
2)install Metricbeat
3)install Packetbeat
3)install Auditbeat
                
                      ''')

                service = input("\033[1;32mWhat do you want to install ?> \033[1;m")
                
                
                
                if(service=='1'):
                    InstallWinlogBeatWindows(my_current_directory,responsefind,findir)

                        
                        
                    
                

            while (os_choice =='2'):
                print('''
1)install Filebeat
2)install Metricbeat
3)install Packetbeat
3)install Auditbeat
                
                      ''')

                service = input("\033[1;32mWhat do you want to install ?> \033[1;m")
                if(service == '1'):
                    InstallFilebeatLinux(my_current_directory,responsefind,findir)
                if(service=='2'):
                    InstallMetricbeatLinux(my_current_directory,responsefind,findir)
                if(service=='3'):
                    InstallPacketbeatLinux(my_current_directory,responsefind,findir)                    
                if(service=='4'):
                    InstallAuditbeatLinux(my_current_directory,responsefind,findir)                     
                    
######################################################################################################Configuration#########################################################################################################################                       

                    
                    
def Configuration(file_path,pattern,subst,serviceconf,i):
    
    
    #Create temp file
   
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if (line.startswith('  hosts:') and i==0):
                    print('cool')
                    new_file.write(f'''  
  topic:"{serviceconf}"
  max_message_bytes: 1000000
''')
                new_file.write(line.replace(pattern,subst))
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)


                



   

######################################################################################################InstallWinlogBeatWindows#########################################################################################################################                       
def InstallWinlogBeatWindows(my_current_directory,responsefind,findir):
    
    if(os.path.exists(my_current_directory+'/winlogbeat.zip')):
        os.remove(my_current_directory+'/winlogbeat.zip')
                            
                        
                        
    if(responsefind):
        print(responsefind,findir)
        shutil.rmtree(my_current_directory+'/'+findir)
                            
                        
    print('Beginning file download with urllib2...')

    url = 'https://artifacts.elastic.co/downloads/beats/winlogbeat/winlogbeat-7.3.2-windows-x86_64.zip'
    urllib.request.urlretrieve(url,my_current_directory+'/winlogbeat.zip')
    file_name=my_current_directory+'/winlogbeat.zip'
    with ZipFile(file_name,'r') as zip:
        zip.printdir()
        print('Extracting all the files now...')
        zip.extractall(path=my_current_directory)
        print('done')
                        

######################################################################################################InstallFilebeatLinux#########################################################################################################################                       
def InstallFilebeatLinux(my_current_directory,responsefind,findir):
    
    CheckFileAndDirectory(my_current_directory,'filebeat.tar.gz',responsefind,findir)
                            
                        
    print('Beginning file download with urllib2...')

    url = 'https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.3.2-linux-x86_64.tar.gz'
    urllib.request.urlretrieve(url,my_current_directory+'/filebeat.tar.gz')
    file_name=my_current_directory+'/filebeat.tar.gz'
    if (file_name.endswith("tar.gz")):
        tar = tarfile.open(file_name, "r:gz")
        tar.extractall()
        tar.close()
    kafka_ip = input("\033[1;32mKafka IP Address: ?> \033[1;m")
    kafka_port = input("\033[1;32mKafka Port ?> \033[1;m")
    pattern="  hosts: [\"localhost:9200\"]"
    subst="  hosts: [\"%s:%s\"]"%(kafka_ip,kafka_port)
    path=my_current_directory+"/filebeat-7.3.2-linux-x86_64/filebeat.yml"
    Configuration(path,pattern,subst,'filebeat',i=0)
    Configuration(path,"output.elasticsearch","output.kafka",'filebeat',i=1)
    

######################################################################################################InstallMetricbeatLinux#########################################################################################################################                       
def InstallMetricbeatLinux(my_current_directory,responsefind,findir):
    
    CheckFileAndDirectory(my_current_directory,'metricbeat.tar.gz',responsefind,findir)
                            
                        
    print('Beginning file download with urllib2...')

    url = 'https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.3.2-linux-x86_64.tar.gz'
    urllib.request.urlretrieve(url,my_current_directory+'/metricbeat.tar.gz')
    file_name=my_current_directory+'/metricbeat.tar.gz'
    if (file_name.endswith("tar.gz")):
        tar = tarfile.open(file_name, "r:gz")
        tar.extractall()
        tar.close()
    kafka_ip = input("\033[1;32mKafka IP Address: ?> \033[1;m")
    kafka_port = input("\033[1;32mKafka Port ?> \033[1;m")
    pattern="  hosts: [\"localhost:9200\"]"
    subst="  hosts: [\"%s:%s\"]"%(kafka_ip,kafka_port)
    path=my_current_directory+"/metricbeat-7.3.2-linux-x86_64/metricbeat.yml"
    Configuration(path,pattern,subst)
    Configuration(path,"output.elasticsearch","output.kafka",'metricbeat',i=1)
    


######################################################################################################InstallPacketbeatLinux#########################################################################################################################                       
def InstallPacketbeatLinux(my_current_directory,responsefind,findir):
    
    CheckFileAndDirectory(my_current_directory,'packetbeat.tar.gz',responsefind,findir)
                            
                        
    print('Beginning file download with urllib2...')

    url = 'https://artifacts.elastic.co/downloads/beats/packetbeat/packetbeat-7.3.2-linux-x86_64.tar.gz'
    urllib.request.urlretrieve(url,my_current_directory+'/packetbeat.tar.gz')
    file_name=my_current_directory+'/packetbeat.tar.gz'
    if (file_name.endswith("tar.gz")):
        tar = tarfile.open(file_name, "r:gz")
        tar.extractall()
        tar.close()
    kafka_ip = input("\033[1;32mKafka IP Address: ?> \033[1;m")
    kafka_port = input("\033[1;32mKafka Port ?> \033[1;m")
    pattern="  hosts: [\"localhost:9200\"]"
    subst="  hosts: [\"%s:%s\"]"%(kafka_ip,kafka_port)
    path=my_current_directory+"/packetbeat-7.3.2-linux-x86_64/packetbeat.yml"
    Configuration(path,pattern,subst)
    Configuration(path,"output.elasticsearch","output.kafka",'packetbeat',i=1)
    
######################################################################################################InstallAuditbeatLinux#########################################################################################################################                       
def InstallAuditbeatLinux(my_current_directory,responsefind,findir):
    
    CheckFileAndDirectory(my_current_directory,'auditbeat.tar.gz',responsefind,findir)
                            
                        
    print('Beginning file download with urllib2...')

    url = 'https://artifacts.elastic.co/downloads/beats/auditbeat/auditbeat-7.3.2-linux-x86_64.tar.gz'
    urllib.request.urlretrieve(url,my_current_directory+'/auditbeat.tar.gz')
    file_name=my_current_directory+'/auditbeat.tar.gz'
    if (file_name.endswith("tar.gz")):
        tar = tarfile.open(file_name, "r:gz")
        tar.extractall()
        tar.close()
    kafka_ip = input("\033[1;32mKafka IP Address: ?> \033[1;m")
    kafka_port = input("\033[1;32mKafka Port ?> \033[1;m")
    pattern="  hosts: [\"localhost:9200\"]"
    subst="  hosts: [\"%s:%s\"]"%(kafka_ip,kafka_port)
    path=my_current_directory+"/auditbeat-7.3.2-linux-x86_64/auditbeat.yml"
    Configuration(path,pattern,subst)
    Configuration(path,"output.elasticsearch","output.kafka",'auditbeat',i=1)

######################################################################################################CheckFileAndDirectory#########################################################################################################################                       

def CheckFileAndDirectory(my_current_directory,remove_file,responsefind,findir):
    
    if(os.path.exists(my_current_directory+remove_file)):
        os.remove(my_current_directory+'/'+remove_file)
                            
    if(responsefind):
        shutil.rmtree(my_current_directory+'/'+findir)
   
######################################################################################################search_directory#########################################################################################################################                       
   
        
def search_directory(path):
    test= bool,''
    for root, directories, files in os.walk(path): 
        for directory in directories:
            if('winlogbeat' in directory ):
                print(directory)
                test = True,directory
        k,l=test
        if(k==True):
            return True,l
        else:
            return False,'lol'
        
                
                
                
                
main()
    
    
